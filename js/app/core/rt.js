﻿(function ($) {

    $.rt = (function (my) {
        var signalRscript, signalRHub, aliveVal;
        var urlHub, clientsHub;
        var $hubState;
        var stateConversion = { 0: 'connecting', 1: 'connected', 2: 'reconnecting', 4: 'disconnected' };

        my.load = function () {
           // console.log('rt.load');
            signalRHub = document.createElement('script');
            signalRHub.type = 'text/javascript';
            signalRHub.async = false;
            signalRHub.src = $.appParms.urlHub();
            (document.getElementsByTagName('body')[0]).appendChild(signalRHub);
        };

        my.start = function () {
            console.log('rt.start');
            //========================================================
            urlHub = $.appParms.urlHub();           
            $.connection.hub.url = urlHub;
            $.connection.hub.logging = false //true; 
            clientsHub = $.connection.clientsHub;

            //console.log('clientsHub', clientsHub);
            //array of device


            /*
            receive messages
            ------------------------------------------------------------------------------*/
            clientsHub.client.device_changed = function (message) {
                try {
                    // console.log("device_changed", message);
                    $.fn.device_changed(message);
                }
                catch (err) {
                    // console.log("device_changed error", err);
                }
            }
           
            /*----------------------------------------------------------------------------*/
            /*connection manager*/
            $.connection.hub.stateChanged(connectionStateChanged);            
            $.connection.hub.start()
            .done(function () {
                console.log('Now connected, connection ID=' + $.connection.hub.id + ' subscriptionId=' + localStorage.getItem('signalIR'));
                clientsHub.server.subscribe(localStorage.getItem('signalIR'));
            })
            .fail(function () {
                console.log('Could not Connect!');
                //toastr["warning"](langResources['serverlost'], langResources['alert']);
            });

            $.fn.unsubscribe = function () {
                try {
                    clientsHub.server.unsubscribe(localStorage.getItem('signalIR'));
                }
                catch (err) {
                    //console.log(err);
                }
                $.connection.hub.stop();
                console.log('connection stop');
            }

            /*send messages*/


            //========================================================
        }; // start

        function connectionStateChanged(state) {
            console.log('SignalR state changed from: ' + stateConversion[state.oldState] + ' to: ' + stateConversion[state.newState]);

            $hubState = state.newState;

            if (stateConversion[state.newState] == 'connected') { connectionStatus(true); }
            if (stateConversion[state.newState] == 'disconnected') { connectionStatus(false); }
            if (stateConversion[state.newState] == 'reconnecting') { connectionStatus(false); }

            if (stateConversion[state.newState] == 'disconnected' && stateConversion[state.oldState] == 'connected') {
                ;
            }
            if (stateConversion[state.newState] == 'connected' && stateConversion[state.oldState] == 'disconnected') {
                start();
            }
        } //connectionStateChanged

        function connectionStatus(status) {
            var data = [{ status: status }];
            console.log('connectionStatus= ' + status);
            $('#navbar-signalR').empty();
            try {
                $("#tmplnavbar-signalR").tmpl(data).appendTo("#navbar-signalR");
            }
            catch (err) {
                // console.log(err);
            }
        }

        my.stop = function () {
            try {
                //unsubscribe();
                clientsHub.server.unsubscribe(localStorage.getItem('signalIR'));
                $.connection.hub.stop();
                console.log("=======================");
                console.log('connection stop');
                console.log("=======================");
            }
            catch (err) {
                // console.log("rt.stop error", err);
            }
        };

        return my;
    })({});

})(jQuery);