(function ($) {
    $.module = (function (my) {
        var $currentModule = '';
        var $errors = [];
        var $counter = 0 ;
		/**************************************/
		/* JSON MENU */
		/*************************************/
		$issue = [
			{
				"nome":"Segnalazioni",
				"icona":"COMP01",
				"status":"0",
				"count":"4",
				"nlev": 0,
				"href":"$.module.load('list', 'issues', 'none');"
			}
		];
		$ticket = [
			{
				"nome":"Ticket",
				"icona":"COMP01",
				"status":"0",
				"count":"4",
				"nlev": 0,
				"href":" $.module.load('list', 'tickets', 'none'); "
			}
		];
		$utentis = [
			{
				"nome":"Utenti",
				"icona":"COMP01",
				"status":"0",
				"count":"4",
				"nlev": 0,
				"livelli": {
					"Crea Nuovo": "localStorage.removeItem('id');$.module.load('anagrafica/create', 'utenti')",
					"Tutti": "$.module.load('anagrafica/list', 'utenti', '');"
				},
				"href":"$.module.load('list', 'users', 'none');"
			}
		];
		$calendar = [
			{
				"nome":"Calendario",
				"icona":"COMP01",
				"status":"0",
				"count":"4",
				"nlev": 0,
				"href":" $.module.load('list', 'events', 'none');"
			}
		];
        $maps = [
            {
                "nome":"Mappa",
                "icona":"COMP01",
                "status":"0",
                "count":"4",
                "nlev": 0,
                "href":" $.module.load('list', 'maps', 'none');"
            }
        ];
        $settings = [
            {
                "nome":"Settings",
                "icona":"COMP01",
                "status":"0",
                "count":"4",
                "nlev": 0,
                "href":" $.module.load('list', 'settings', 'none');"
            }
        ];
        $ricerca = [
            {
                "nome":"Ricerca",
                "icona":"COMP01",
                "status":"0",
                "count":"4",
                "nlev": 0,
                "href":" $.module.load('list', 'search', 'none');"
            }
        ];
        $files = [
            {
                "nome":"Files",
                "icona":"COMP01",
                "status":"0",
                "count":"4",
                "nlev": 0,
                "href":" $.module.load('list', 'files', 'none');"
            }
        ];

//========================================================================================
//Menu Loader
//========================================================================================
		my.loadMenu = function (type){
		
			if (localStorage.role == "admin" ) $menuNew = { "issue" : $issue, "ticket" : $ticket, "utenti" : $utentis ,"calendar" : $calendar,"settings" : $settings, "search":$ricerca, "maps":$maps};
			else if (localStorage.role == "operator") $menuNew = { "issue" : $issue, "ticket" : $ticket, "utenti" : $utentis ,"calendar" : $calendar,"settings" : $settings, "search":$ricerca, "maps":$maps};
			else if (localStorage.role == "app-user" ) $menuNew = { "issue" : $issue, "ticket" : $ticket, "utenti" : $utentis ,"calendar" : $calendar,"settings" : $settings, "search":$ricerca, "maps":$maps}
			else $menuNew = { "issue" : $issue, "ticket" : $ticket, "utenti" : $utentis ,"calendar" : $calendar,"settings" : $settings, "search":$ricerca, "maps":$maps};
			
			
				switch(type) {
                    case "admin":
                        jQuery.ajax( 'modules/templates/menuTemplates.html', {
                            type: 'GET',
                            cache: false,
                            async: false,
                            dataType: 'html'
                        }).done(function (response) {
                            console.log("carico menu in module",$menuNew, localStorage.role);
                            $('#module').append(response);
                            $('#side-menu').empty();
                            $( "#side-menu" ).append($("#tmplside-menu2" ).render( $menuNew ));
                            $("#"+ $rootingStore.getState().modulo + "Menu").addClass('active');
                        });
                        localStorage.menuType="default";
                        break;
                    case "operatore":
                        jQuery.ajax( 'modules/templates/menuTemplates.html', {
                            type: 'GET',
                            cache: false,
                            async: false,
                            dataType: 'html'
                        }).done(function (response) {
                            console.log("carico menu in module",$menuNew, localStorage.role);
                            $('#module').append(response);
                            $('#side-menu').empty();
                            $( "#side-menu" ).append($("#tmplside-menu2" ).render( $menuNew ));
                            $("#"+ $rootingStore.getState().modulo + "Menu").addClass('active');
                        });
                        localStorage.menuType="default";
                        break;
					
					default:
						console.log ("switchDefaultCase");
					break;
				}
		};


/**************************************/
/* Routing  di base */
/*************************************/
        my.load = function (methods, modulo='issues', params="") {

            $rootingStore.updateState(function(state) {
                state.methods = methods;
                state.modulo = modulo;
                state.params = params;
            }, 'update store');
            // localStorage.methods = methods;
            // localStorage.modulo = modulo;
			// localStorage.params = JSON.stringify(params);
			console.log("METHOD>>>",methods, "MODULO>>>>", modulo);
			var module2load = '';
			module2load = modulo + '.html';
			d = new Date();
			$("body").addClass("loading");
            switch(methods) {
                case "list":
                    $("#module").fadeOut(100,function () {
                        // interoia evita la chace del browser
                        jQuery.ajax( 'modules/'+ modulo + "/" + module2load + "?d=" + d.getTime() , {
                            type: 'GET',
                            cache: false,
                            async: false,
                            dataType: 'html'
                        }).done(function (response) {
                            $('#module').unbind();
                            $("#modals").unbind();
                            $('#module').off();
                            $("#modals").off();
                            $('#module').remove();
                            $( "#modals" ).after( "<div id='module'></div>");
                            $('#module').empty();
                            $('#module').html(response);
                            $("#module").fadeIn(100);
                            //Check for ACL
                        });
                    });

                    break;
                case "edit":
                    $("#listHolder").fadeOut(100,function () {
                        $.when(   $.module.read($rootingStore.getState().modulo, $rootingStore.getState().id) )
                            .then(
                                function( data ) {
                                    console.log("read singolo dopo promessa", data);
                                    var myTmpl =$.templates[$rootingStore.getState().modulo+"Edit"];
                                    console.log("myTmpl", $rootingStore.getState().modulo);
                                    $( "#editHolder" ).append(
                                        myTmpl.render({"items":"edit", "data":data})
                                    );
                                    $.module.compileForm(data);
                                    $("#editHolder").fadeIn(100);
                                    $itemsStore.updateState(function(state) {
                                        state.itemC = data;
                                    }, 'updateItemC');

                                }
                            );
                    });
                    break;
                case "create":
                    $("#listHolder").fadeOut(100,function () {

                                    var myTmpl =$.templates[$rootingStore.getState().modulo+"Edit"];
                                    $( "#editHolder" ).append(
                                        myTmpl.render({"items":"create"})
                                    );
                                    $("#editHolder").fadeIn(100);
                    });

                    break;
                case "createExt":
                    $("#listHolder").fadeOut(100,function () {
                        var myTmpl =$.templates[$rootingStore.getState().modulo+"Edit"];
                        $( "#editHolder" ).append(
                            myTmpl.render({"items":"create"})
                        );
                        $("#editHolder").fadeIn(100);
                        $.when(   $.module.issueTicket('issues',localStorage.issueForTicketid ) )
                            .then(
                                function( data ) {
                                    console.log("data", data);
                                    //caso particolare delle liste le gestisco tutto qui. manipolazioni di dati
                                    var myTmpl =$.templates[$rootingStore.getState().modulo+"ListIssues"];
                                    $( "#listIssues" ).append(
                                        myTmpl.render(data)
                                    );

                                }
                            );
                    });

                    break;

                default:
                    console.log ("switchDefaultCase");
                    break;
            }

        };

/**************************************/
/* GO BACK GENERAL */
/*************************************/
        my.goBack = function () {
            var methods = $rootingStore.getState().methods;
            switch(methods) {
                case "list":
                    if (localStorage.idPadre){
                        var padre = localStorage.idPadre;
                        localStorage.removeItem ("idPadre");
                        localStorage.id = padre;
                        localStorage.module = "anagrafica/list";
                        localStorage.modulo = localStorage.preModulo;
                        my.load (localStorage.preModule, localStorage.preModulo,JSON.parse(localStorage.preParams) );
                    }
                    else my.load (localStorage.preModule, localStorage.preModulo, JSON.parse(localStorage.preParams));

                    break;
                case "edit":
                    $("#editHolder").fadeOut(100);
                    $('#editHolder').unbind();
                    $('#editHolder').off();
                    $('#editHolder').remove();
                    $( "#listHolder" ).after( "<div class=\"row\" id='editHolder'></div>");
                    var updItem = $itemsStore.getState().item;
                    var myTmpl =$.templates[$rootingStore.getState().modulo+"Row"];

                    $( "#"+ updItem.id ).html(
                        myTmpl.render( updItem )
                    );
                    $("#listHolder").fadeIn(100);
                    break;
                case "create":
                    $("#editHolder").fadeOut(100);
                    $('#editHolder').unbind();
                    $('#editHolder').off();
                    $('#editHolder').remove();
                    $( "#listHolder" ).after( "<div class=\"row\" id='editHolder'></div>");
                    $("#listHolder").fadeIn(100);
                    break;
                default:
                    console.log ("switchDefaultCase");
                    break;
            }
        };



/**************************************/
/* ACL di base */
/*************************************/
        my.checkPrivileges = function () {


		}

/**************************************/
/* Validation input Odin EDIT */
/*************************************/


/**************************************/
/* DELETE DI BASE E INNER */
/*************************************/
        my.del = function (module, id) {
			// $("#module").on("click", "#delBtn", function () {
			swal({
                title: "Sei sicuro?",
                text: "L'elemento verrà cancellato definitivamente",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function(){
               var req = $.DataAccess.del(module, id);
				req.success(function (json) {
					console.log ("deleted data", json);

					$("#"+id).fadeOut();

					$.toast({
                        heading: "Elemento di " +$rootingStore.getState().modulo + " Cancellato",
                        text: '',
                        position: 'top-left',
                        loaderBg:'#ff6849',
                        icon: 'info',
                        hideAfter: 3500,

                        stack: 6
                    });

				});
			})
		};


        my.delInpage = function (module, id) {
            // $("#module").on("click", "#delBtn", function () {
            swal({
                title: "Sei sicuro?",
                text: "L'elemento verrà cancellato definitivamente",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function(){
                var req = $.DataAccess.del(module,  id);
                req.success(function (json) {
                    console.log ("deleted data", json);
                    $("#"+id+"Row").fadeOut();
                    $.toast({
                        heading: "Elemento di " +$rootingStore.getState().modulo+ " Cancellato",
                        text: '',
                        position: 'top-left',
                        loaderBg:'#ff6849',
                        icon: 'info',
                        hideAfter: 3500,

                        stack: 6
                    });


                });
            })
        }


/**************************************/
/* Autocompilatore di form base*/
/*************************************/
        my.compileForm = function (data){
            $("input").each(function( replace ) {
                var pointer = $(this).attr("name");
                 // console.log("pointer", pointer  );
                if ( (pointer == "start") || (pointer =="end") ){
                    var date = moment(data[pointer]).local().format("DD-MM-YYYY");
                    data[pointer] = date;
                }
                $(this).val(_.get(data, pointer));
            });

            $("textarea").each(function( replace ) {
                var pointer = $(this).attr("name");
                $(this).val(_.get(data, pointer));
            });

            $("select").each(function( replace ) {
                var pointer = $(this).attr("name");
                $(this).val(_.get(data, pointer));
                $("#"+data[pointer]).show();

            });
        };


/**************************************/
/* retrive data */
/*************************************/
        my.savedata = function (){
            var dfd = jQuery.Deferred();
            var savedata = {
                issueIds : [],
            };

            $('.takeIn').each(function () {
                var name = $(this).attr("name");
                var value = $(this).val();
                _.set(savedata, name, value);
                // savedata[name] = value;
            });

            $('.takeIn2').each(function () {
                var id = $(this).attr("data-id");
                savedata.issueIds.push(id)
            });
            console.log("DATI SALVATI MODULE", savedata);
            dfd.resolve(savedata);
            return dfd
        };
/**************************************/
/* update  form di base*/
/*************************************/
        my.update = function (modulo, id, savedata){
            console.log("update data", savedata);
            var req = $.DataAccess.update(modulo, id, savedata);
            req.success(function (json) {
                $.toast({
                    heading: "<h3 style='color:white; text-transform: uppercase'>"+$rootingStore.getState().modulo+"</h3>",
                    text: 'Dati salvati con successo',
                    position: 'top-left',
                    loaderBg:'#ff6849',
                    icon: 'info',
                    hideAfter: 3500,

                    stack: 6
                })
                var data = json;
                console.log ("update data", data);
                $itemsStore.updateState(function(state) {
                    state.item = data;
                }, 'updateItem');
            });
        };
/**************************************/
/* create  form di base*/
/*************************************/
        my.create = function (modulo, createdata){
            console.log("createdata", createdata);
            var req = $.DataAccess.create(modulo, createdata);
            req.success(function (json) {
                $.toast({
                    heading: "<h3 style='color:white; text-transform: uppercase'>"+$rootingStore.getState().modulo+"</h3>",
                    text: 'Dati creati con successo',
                    position: 'top-left',
                    loaderBg:'#ff6849',
                    icon: 'info',
                    hideAfter: 3500,

                    stack: 6
                })
                var data = json;
                console.log ("createdata", data);
                var myTmpl =$.templates[$rootingStore.getState().modulo+"Edit"];
                $( "#editHolder" ).html(
                    myTmpl.render({"items":"edit", "data" : data} )
                );
                // $( "#editHolder" ).html(
                //     $( "#"+$rootingStore.getState().modulo +"Edit" ).render( {"items":"edit"} )
                // );
                $itemsStore.updateState(function(state) {
                    state.item = data;
                }, 'updateItem');
                $.module.compileForm(data);

            });
        };
/**************************************/
/* list  form di base*/
/*************************************/
        my.list = function (modulo, params){
            var dfd = jQuery.Deferred();
            var req1 = $.DataAccess.list(modulo, params );
            req1.success(function (json) {
                var data = json;
                console.log("read lista db", data);
                dfd.resolve(data);
            });
            return dfd
        };
/**************************************/
/* list  form di base*/
/*************************************/
        my.listTicket = function (modulo, params, role, department){

            var dfd = jQuery.Deferred();
            var req1 = $.DataAccess.listTicket(modulo, params, role, department);
            req1.success(function (json) {
                var data = json;
                console.log("read lista db per ticket", data);
                dfd.resolve(data);
            });
            return dfd
        };
/**************************************/
/* read  form di base*/
/*************************************/
        my.read = function (modulo, id){
            var dfd = jQuery.Deferred();
            var req1 = $.DataAccess.read(modulo, id );
            req1.success(function (json) {
                var data = json;
                console.log("edit singolo", data);
                dfd.resolve(data);
            });
            return dfd
        };
/**************************************/
/* update della segnalazione nel ticket*/
/*************************************/
        my.issueTicket= function (modulo, id){
            var dfd = jQuery.Deferred();
            var req1 = $.DataAccess.read(modulo, id );
            req1.success(function (json) {
                var data = json;
                console.log("issue for ticket", data);
                dfd.resolve(data);
            });
            return dfd
        };

/**************************************/
/* statistiche*/
/*************************************/
        my.ticketStat= function (modulo, id){
            var dfd = jQuery.Deferred();
            var req1 = $.DataAccess.ticketStat('none', 'none' );
            req1.success(function (json) {
                var data = json;
                console.log("tickets stat", data);
                dfd.resolve(data);
            });
            return dfd
        };

        my.addIssues = function () {
            $.when(   loadissue() )
                .then(
                    function( data ) {
                        console.log("issues", data);
                        $('#generic-modal').modal('show');
                        $("#InModal").empty();
                        $(".modal-title").text("Assegna Le Segnalazioni al ticket");
                        // $("#InModal").append(
                        //     $("#" + template + "tutto").render(prova)
                        // );
                        var myTmpl =$.templates[$rootingStore.getState().modulo+"Modal"];
                        $( "#InModal" ).append(
                            myTmpl.render({"items":data} )
                        );
                    }
                );
        };
        my.addIssuesRow = function (id, category, description) {
            // console.log("issuesAdd",issuesAdd);
            var itemIss = {
                _id:id,
                category :category,
                description:description
            }
            var myTmpl =$.templates[$rootingStore.getState().modulo+"ListIssues"];
            $( "#listIssues" ).append(
                myTmpl.render(itemIss)
            );
        }



        function loadissue() {
            var dfd = jQuery.Deferred();
            var req2 = $.DataAccess.listDuplicate("issues", "none", localStorage.issueForTicketid);
            req2.success(function (json) {
                var utente = json;

                dfd.resolve(utente);
                // compileForm(data);
            });
            return dfd;

        }


        return my;
    })({});
	
    })(jQuery);