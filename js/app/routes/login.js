$(function () {
    var langResources = new Array();
    //global functions
    //===========================================

    $(document).ready(function () {

        if ($userStore.getState().rememberMe) {
           //$('#login-username').val(  localStorage.username);
           // $('#login-password').val(  localStorage.pass );
            $('#checkbox-signup').attr('checked', 'checked');
        }

        $('#checkbox-signup').click(function() {
            // if ($('#checkbox-signup').is(':checked')) {
            //     // save username and password
            //
            //
            // } else {
            //     localStorage.clear();
            //     localStorage.setItem("CurrentLanguage", "en");
            // }
            var rememberMe = $(this).is(':checked');
            $userStore.updateState(function(state) {
                state.rememberMe = rememberMe;
            }, 'rememberMeChecked');
        });

        if (!localStorage.getItem('CurrentLanguage')) {
            localStorage.setItem("CurrentLanguage", "en");
            //langResources = $.languages.get()["en"];
        }
        else {
            //langResources = $.languages.get()[localStorage.CurrentLanguage];
            //langResources = $.languages.get()["en"];
            localStorage.setItem("CurrentLanguage", "en");
        }

        setlanguage();
    });
    //lang
    //==========================================
    //langEn
    $('#langEn').on('click', function (e) {
        localStorage.setItem("CurrentLanguage", "en");
        //langResources = getLanguageResources()["en"];
        langResources = $.languages.get()["en"];
        setlanguage();
    });
    //langIt
    $('#langIt').on('click', function (e) {
        window.localStorage.setItem("CurrentLanguage", "it");
        //langResources = getLanguageResources()["it"];
        langResources = $.languages.get()["it"];
        setlanguage();
    });
    function setlanguage() {
        $("span[name='lbl']").each(function (i, elt) {
            try {
                $(elt).text(langResources[$(elt).attr("caption")]);
                if ($(elt).parent().attr('for')) {
                    var xxx = $(elt).parent().attr('for');
                    $('#' + xxx).attr('placeholder', $(elt).text());
                    //console.log(xxx);
                }

            }
            catch (err) {
                console.log(err);
            }
        });
    }
    //==========================================

    //login
    //===========================================
    $('#login_btn').bind('click', function (e) {
        Login(e);
    });
	function Login(e) {

            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }

            var username = $('#login-username').val().trim(),
                password = $('#login-password').val().trim();



        if ($('#checkbox-signup').is(':checked')) {
            // save username and password
            localStorage.username = $('#login-username').val();
            // localStorage.pass = $('#login-password').val();
            localStorage.chkbx = true;
        }

        if (username === "" || password === "") {
            $.toast({
                heading: 'LOGIN',
                text: 'Entrambi i campi sono richiesti',
                position: 'top-left',
                loaderBg:'#ff6849',
                icon: 'info',
                hideAfter: 1500,

                stack: 6
            });
        } else {
            msg = {
                "username": username,
                "password": password
            }

            var req3 = $.DataAccess.login(username,password);
            req3.success(function (json) {
                var login = json;
                console.log("login", login);
                if (login) {
                    $userStore.updateState(function(state) {
                        state.token = login.token;
                        state.userId = login.user._id;
                        state.role = login.user.role;
                        state.username = login.user.email;
                        state.userMail = login.user.email;
                        state.department = login.user.department;
                        state.preLastLogin = moment(login.pre_last_login).local().format("DD-MM-YYYY HH:mm");
                    }, 'userLoggedIn');



                    localStorage.setItem("username", login.user.email);
                    localStorage.setItem("userId", login.user._id);
                    localStorage.setItem("role", login.user.role);
                    localStorage.setItem("userMail", login.user.email);
                    localStorage.setItem("token", login.token);
					localStorage.setItem("preLastLogin", moment(login.pre_last_login).local().format("DD-MM-YYYY HH:mm"));
                    //$.module.load('Impianti/'+localStorage.role+'/projects');
                    var whereigo = '';
                    whereigo = 'struttura/struttura.html';
					setTimeout(function(){
						$.router.navigate(whereigo);
					}, 3);
                }
                else {
                    alert("gwt non tradotto");
                }

            });
		}
	}
    function LoginOld(e) {

            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }

            var username = $('#login-username').val().trim(),
                password = $('#login-password').val().trim();



        if ($('#checkbox-signup').is(':checked')) {
            // save username and password
            localStorage.username = $('#login-username').val();
            // localStorage.pass = $('#login-password').val();
            localStorage.chkbx = true;
        }

        if (username === "" || password === "") {
            $.toast({
                heading: 'LOGIN',
                text: 'Entrambi i campi sono richiesti',
                position: 'top-left',
                loaderBg:'#ff6849',
                icon: 'info',
                hideAfter: 1500,

                stack: 6
            });
        } else {
            msg = {
                "username": username,
                "password": password
            }
            //console.log("messagio", msg);
            var req = $.sso.login( msg);
            req.success(function (json) {
                //console.log("json", json);
                if (json) {
                    var data = json.token;
                    //console.log("login", data);
                    localStorage.setItem("token", data);
                    var req = $.sso.gwtTransale();
                    req.success(function (json) {
                        if (json) {
                            var data = json.message.user;
                        console.log("in login data", data);
                            localStorage.setItem("username", data.username);
                            localStorage.setItem("role", data.role[0]);
                         
                            //$.module.load('Impianti/'+localStorage.role+'/projects');
                            var whereigo = '';
                            whereigo = 'struttura/struttura.html';
                            $.router.navigate(whereigo);
                        }
                        else {
                           alert("gwt non tradotto");
                        }
                    });
                }
                else {
                    $.toast({
                        heading: 'LOGIN',
                        text: 'login non riuscito',
                        position: 'top-left',
                        loaderBg:'#ff6849',
                        icon: 'info',
                        hideAfter: 1500,

                        stack: 6
                    });

                }

            });
        }
    }
    //===========================================




    //===========================================
});
