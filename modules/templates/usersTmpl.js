$.templates("usersList",
    `<div class="col-sm-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <span class="page-title">LISTA UTENTI</span>
			<a href="javascript: $.module.load('create','users');" ><i style="font-size: 21px;vertical-align: -4px;" class="icon-plus"></i></a>
            <div class="pull-right">
                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                <!-- <a href="javascript:goBack();" data-perform=""><i class="ti-close"></i></a> -->
            </div>
        </div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div  class="panel-body">

                    <table style="font-size:14px;" id="GenericTable" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Ruolo</th>
                                <th>Dipartimento</th>
                                <th>Ultimo Accesso</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                        <tbody>
                         {{for  items  }}
                             <tr id='{{:id}}Row'>
                                 <td><span> <img style="width:30px; height:30px" src="{{:picture onerror=''}}" alt="user" class="img-circle"></span>  <span>{{:email onerror=''}}</span></td>
                                 <td> {{:role onerror=''}}</td>
                                 <td> {{:department  onerror=''}}</td>
                                 <td> {{:last_login    onerror=''}}</td>
                                 <td>
                                    <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','users');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a data-role='admin'  href="javascript:$.module.del('users','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                         {{/for}}
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>`
);


$.templates("usersEdit",
    `<div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading"> <span name="lbl" caption="anagraficalead">Anagrafica Utenti</span>
                <div class="pull-right">
                    <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
	                <a href="javascript:$.module.goBack();" data-perform=""><i class="ti-close"></i></a>
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-body">
                            {{if items == "create"}}
                            <!------------------------------>
                            <!--ACCOUNT CREATE-->
                            <!------------------------------>
                            <h3 class="box-title">Account</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mail/Username</label>
                                        <div class="col-md-9">
                                            <input id="email" name="email" type="text"  class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Password</label>
                                        <div class="col-md-9">
                                            <input id="password" name="password" type="password" class="form-control takeIn" value="*****************" >
                                        </div>
                                    </div>
                                </div>
                                                                                      

                            </div>
                            {{else item=="edit}}
                            <!------------------------------>
                            <!--ACOCUNT EDIT-->
                            <!------------------------------>
                            <h3 class="box-title">Account</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mail/Username</label>
                                        <div class="col-md-9">
                                            <input id="email" name="email" type="text"  class="form-control takeIn" disabled>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Ultimo Accesso</label>
                                        <div class="col-md-9">
                                            <input id="last_login" name="last_login" type="text"  class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                               
                              

                            </div>
                            {{/if}}
                        
                            <!------------------------------>
                            <!--ROW  RUOLI E PERMESSI-->
                            <!------------------------------>
                            <h3 class="box-title">Ruoli e Permessi</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Disabilita Utente</label>
                                        <div class="col-md-9">
                                            <input type="checkbox" class="custom-control-input" id='isEnabled' name='isEnabled'>
                                            <!-- <span class="help-block"> primo nome</span>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Ruolo</label>
                                        <div class="col-md-9">
                                            <select id="role" name="role" type="text" class="form-control takeIn" placeholder="">
                                                <option value="admin">Admin</option>
                                                <option value="operatore">Operatore</option>
                                                <option value="app-user"> app-user</option>                                                                                    
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Dipartimento</label>
                                        <div class="col-md-9">
                                            <select id="department" name="department" type="text" class="form-control takeIn" placeholder="">
                                                <option value="Lavori pubblici">Lavori pubblici</option>
                                                <option value="Innovazione">Innovazione</option>
                                                <option value="Sport">Sport</option>
                                                <option value="Politiche per lo sviluppo">Politiche per lo sviluppo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                             <!------------------------------>
                            <!--LIVELLO UTENTE-->
                            <!------------------------------>
                            <h3 class="box-title">Punti e Premi</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                              
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-6">Punti Esperienza</label>
                                        <div class="col-md-6">
                                            <input id="experiencePoints" name="experiencePoints" type="text"  class="form-control takeIn" disabled>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-6">Badge</label>
                                        <div class="col-md-6">
                                            <a style="color:white;" href="javascript: $.fn.save();" type="button" class="btn btn-success "><i class=  ti-target></i> 10 Ticket Risolti !</a>
                                            <a style="color:white;" href="javascript: $.fn.save();" type="button" class="btn btn-danger "><i class=  ti-timer></i> Ticket risolto nel minor tempo </a>
                                        </div>
                                    </div>
                              
                                </div>
                            </div>
                         
                            <!------------------------------>
                            <!--CAMBIO PASSWORD AMMINISTRATORE-->
                            <!------------------------------>
                            {{if items == "edit"}}
                            <div class="row" id="passBlock"  data-block='admin'>
                                <h3 class="box-title">Cambio Password</h3>
                                <hr class="m-t-0 m-b-40">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Admin Password</label>
                                        <div class="col-md-9">
                                            <input id="passActualPassword" name="passActualPassword" type="password" class="form-control" placeholder="" >

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">New Password</label>
                                        <div class="col-md-9">
                                            <input id="passPassword" name="passPassword" type="password" class="form-control" value="" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">New Password Confirmation</label>
                                        <div class="col-md-9">
                                            <input id="passPasswordx" name="passPasswordx" type="password" class="form-control" value="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{/if}}
                            <!------------------------------>
                            <!--AZIONI-->
                            <!------------------------------>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-1 col-md-9">
                                        {{if items == "create"}}
                                        <a href="javascript: $.fn.create();" type="button" class="btn btn-success">Crea</a>
                                        {{else items == "edit"}}
                                        <a href="javascript: $.fn.save();" type="button" class="btn btn-success">Salva</a>
                                        <button id="changePassword" type="button" class="btn btn-warning">Cambia Password</button>
                                        <button id="disableUser" type="button" class="btn btn-danger pull-right userRights">Disabilita Utente</button>
                                        <button id="enableUser" type="button" style='display:none' class="btn btn-success pull-right userRights" >Abilita Utente</button>
                                        {{/if}}
                                        <a href="javascript:$.module.del();" data-role='admin' id="delBtn2" type="button" class="btn btn-default">Cancella</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
 </div>`
);

$.templates("usersRow",
    `<td><span> <img style="width:30px; height:30px" src="{{:picture onerror=''}}" alt="user" class="img-circle"></span>  <span>{{:email onerror=''}}</span></td>
     <td> {{:role onerror=''}}</td>
     <td> {{:isEnabled  onerror=''}}</td>
     <td> {{:last_login    onerror=''}}</td>
     <td>
        <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','users');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
        <a data-role='admin'  href="javascript:$.module.del('users','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </td>`
);

$.templates("usersSample",
    ``
);