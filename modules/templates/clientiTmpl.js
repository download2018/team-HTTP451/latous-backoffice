$.templates("clientsList",
    `<div class="col-sm-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <span class="page-title">LISTA CLIENTI</span>
			<a href="javascript: $.module.load('create','clients');" ><i style="font-size: 21px;vertical-align: -4px;" class="icon-plus"></i></a>
            <div class="pull-right">
                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                <!-- <a href="javascript:goBack();" data-perform=""><i class="ti-close"></i></a> -->
            </div>
        </div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div  class="panel-body">

                    <table style="font-size:14px;" id="GenericTable" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Codice Cliente</th>
                                <th>Azienda</th>
                                <th>Gruppo</th>
                                <th>Categoria</th>
                                <th>Stato</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                        <tbody>
                         {{for  items  }}
                             <tr id='{{:id}}'>
                                 <td>{{:clientCode onerror=''}}</td>
                                 <td> {{:companyId onerror=''}}</td>
                                 <td> {{:group     onerror=''}}</td>
                                 <td> {{:category  onerror=''}}</td>
                                 <td> {{:status    onerror=''}}</td>
                                 <td>
                                    <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','clients');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a data-role='admin'  href="javascript:$.module.del('clients','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                         {{/for}}
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>`
);


$.templates("clientsEdit",
    `<div class="col-md-12">
        <div class="panel panel-inverse">
             {{if items == "create"}}
                 <div class="panel-heading"> <span name="lbl" caption="anagraficalead">Clients Crea</span>
                    <div class="pull-right">
                        <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                        <a href="javascript:$.module.goBack();" data-perform=""><i class="ti-close"></i></a>
    
                    </div>
                </div>
												   
			 {{else items == "edit"}}
                 <div class="panel-heading"> <span name="lbl" caption="anagraficalead">Clients Modifica</span>
                    <div class="pull-right">
                        <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                        <a href="javascript:$.module.goBack();" data-perform=""><i class="ti-close"></i></a>
    
                    </div>
                </div>											    
			 {{/if}}          
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <form action="#" class="form-horizontal" id='leadForm'>
                        <div class="form-body">
                            <!--row-->
                            <h3 class="box-title">Informazioni Personali </h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Codice Cliente*</label>
                                        <div class="col-md-9">
                                            <input id="clientCode" style="text-transform:uppercase" name="clientCode" type="text" class="form-control takeIn" placeholder="" required>
                                            <!-- <span class="help-block"> This field has error. </span> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Ragione Sociale*</label>
                                        <div class="col-md-9">
                                            <input id="businessName" style="text-transform:uppercase" name="businessName" type="text" class="form-control takeIn" placeholder="" required>
                                            <!-- <span class="help-block"> primo nome</span>  -->
											</div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Categoria*</label>
                                        <div class="col-md-9">
                                            <input id="category" style="text-transform:uppercase" name="category" type="text" class="form-control takeIn" placeholder="" required>
                                            <!-- <span class="help-block"> This field has error. </span> -->
											</div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Gruppo*</label>
                                        <div class="col-md-9">
                                            <input id="group" style="text-transform:uppercase" name="group" type="text" class="form-control takeIn" placeholder="" required>
                                            <!-- <span class="help-block"> This field has error. </span> -->
                                        </div>
                                    </div>
                                </div>

								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Cellulare*</label>
                                        <div class="col-md-9">
                                            <input id="contact.mobileTelephoneNumber" name="contact.mobileTelephoneNumber" type="text" class="form-control takeIn" required >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Telefono</label>
                                        <div class="col-md-9">
                                            <input id="contact.telephoneNumber" name="contact.telephoneNumber" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mail Principale</label>
                                        <div class="col-md-9">
                                            <input id="contact.email" name="contact.email" type="text" class="form-control takeIn">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6" >
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mail Secondaria</label>
                                        <div class="col-md-9">
                                            <input id="contact.secondaryEmail" name="contact.secondaryEmail" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Stato</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <select  id='esito' class="form-control takeIn esitoLead" name='esito'>
													<!--<option value="NON ASSEGNATO">NON ASSEGNATO</option>-->
                                                    <!--<option value="CONTATTO EFFETTUATO">CONTATTO EFFETTUATO</option>-->
                                                    <!--<option value="DA RICONTATTARE">DA RICONTATTARE</option>-->
                                                    <!--<option value="NON RISPONDE">NON RISPONDE</option>-->
                                                    <!--<option value="IN LAVORAZIONE">IN LAVORAZIONE</option>-->
                                                    <!--<option value="CLIENTE">CLIENTE</option>-->
                                                    <!--<option value="NEGATIVO">NEGATIVO</option>-->

                                                </select>
                                                <!-- <span class="input-group-btn"><button class="btn btn-default btn-outline" type="button">+</button></span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Note</label>
										<div class="col-md-9">
											<textarea id="annotations" name="annotations" class="form-control takeIn" rows="5"></textarea>
											<!-- <span class="help-block"> This field has error. </span>  -->
										</div>

									</div>
								</div>
								<div class="col-md-6" data-block='admin'>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Data Inserimento</label>
                                        <div class="col-md-9" data-block='admin'>
                                            <input id="createdAt" name="createdAt" type="text" class="form-control takeIn generalTimePicker" disabled >
                                        </div>
                                    </div>
								</div>
							</div>
                            <!--/row-->
                            <!--row-->
							<h3 class="box-title">Sede Legale</h3>
                            <hr class="m-t-0 m-b-40">
                            <div data-name="legalHeadquarter" class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Indirizzo </label>
                                        <div class="col-md-9">
                                            <input id="legalHeadquarter.address.streetName" name="legalHeadquarter.address.streetName" type="text" class="form-control takeIn">
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Citta</label>
                                        <div class="col-md-9">
                                            <input id="legalHeadquarter.address.city" name="legalHeadquarter.address.city" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Provincia</label>
                                        <div class="col-md-9">
                                            <input maxlength="2" id="legalHeadquarter.address.province" name="legalHeadquarter.address.province" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">CAP</label>
                                        <div class="col-md-9">
                                            <input id="legalHeadquarter.address.zipCode" name="legalHeadquarter.address.zipCode" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
								</div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Regione</label>
                                        <div class="col-md-9">
                                          <div class="input-group">
                                                <select  name='legalHeadquarter.address.region' id='legalHeadquarter.address.region' class="form-control takeIn" >
													<option value=""></option>
                                                    <option value="Abruzzo">Abruzzo</option>
													<option value="Basilicata">Basilicata</option>
													<option value="Calabria">Calabria</option>
													<option value="Campania">Campania</option>
													<option value="Emilia-Romagna>Emilia-Romagna</option>
													<option value="Friuli-Venezia-Giulia">Friuli-Venezia-Giulia</option>
													<option value="Lazio">Lazio</option>
													<option value="Liguria">Liguria</option>
													<option value="Lombardia">Lombardia</option>
													<option value="Marche">Marche</option>
													<option value="Molise">Molise</option>
													<option value="Piemonte">Piemonte</option>
													<option value="Puglia">Puglia</option>
													<option value="Sardegna">Sardegna</option>
													<option value="Sicilia">Sicilia</option>
													<option value="Toscana">Toscana</option>
													<option value="Trentino-Alto-Adige">Trentino-Alto-Adige</option>
													<option value="Umbria">Umbria</option>
													<option value="Valle da Aosta">Valle da Aosta</option>
													<option value="Veneto">Veneto</option>
                                                </select>
                                                <!-- <span class="input-group-btn"><button class="btn btn-default btn-outline" type="button">+</button></span> -->
                                            </div>
                                        </div>
                                    </div>
								</div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nazione</label>
                                        <div class="col-md-9">
                                            <input id="legalHeadquarter.address.state" name="legalHeadquarter.address.state" type="text" class="form-control takeIn">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <!--row-->
                            <h3 class="box-title">Sede Secondaria</h3>
                            <hr class="m-t-0 m-b-40">
                            <div data-name="operationalHeadquarter" class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Indirizzo </label>
                                        <div class="col-md-9">
                                            <input id="operationalHeadquarter.address.streetName" name="operationalHeadquarter.address.streetName" type="text" class="form-control takeIn">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Citta</label>
                                        <div class="col-md-9">
                                            <input id="operationalHeadquarter.address.city" name="operationalHeadquarter.address.city" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Provincia</label>
                                        <div class="col-md-9">
                                            <input maxlength="2" id="operationalHeadquarter.address.province" name="operationalHeadquarter.address.province" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">CAP</label>
                                        <div class="col-md-9">
                                            <input id="operationalHeadquarter.address.zipCode" name="operationalHeadquarter.address.zipCode" type="text" class="form-control takeIn" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Regione</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <select  name='operationalHeadquarter.address.region' id='operationalHeadquarter.address.region' class="form-control takeIn" >
                                                    <option value=""></option>
                                                    <option value="Abruzzo">Abruzzo</option>
                                                    <option value="Basilicata">Basilicata</option>
                                                    <option value="Calabria">Calabria</option>
                                                    <option value="Campania">Campania</option>
                                                    <option value="Emilia-Romagna>Emilia-Romagna</option>
													<option value="Friuli-Venezia-Giulia">Friuli-Venezia-Giulia</option>
                                                    <option value="Lazio">Lazio</option>
                                                    <option value="Liguria">Liguria</option>
                                                    <option value="Lombardia">Lombardia</option>
                                                    <option value="Marche">Marche</option>
                                                    <option value="Molise">Molise</option>
                                                    <option value="Piemonte">Piemonte</option>
                                                    <option value="Puglia">Puglia</option>
                                                    <option value="Sardegna">Sardegna</option>
                                                    <option value="Sicilia">Sicilia</option>
                                                    <option value="Toscana">Toscana</option>
                                                    <option value="Trentino-Alto-Adige">Trentino-Alto-Adige</option>
                                                    <option value="Umbria">Umbria</option>
                                                    <option value="Valle da Aosta">Valle da Aosta</option>
                                                    <option value="Veneto">Veneto</option>
                                                </select>
                                                <!-- <span class="input-group-btn"><button class="btn btn-default btn-outline" type="button">+</button></span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nazione</label>
                                        <div class="col-md-9">
                                            <input id="operationalHeadquarter.address.state" name="operationalHeadquarter.address.state" type="text" class="form-control takeIn">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <!--row-->
							<h3 class="box-title">Documenti</h3>
							<div class="form-check" class="col-md-6">
								<label class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id='checkAzienda' name='isAzienda'>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Spuntare se Azienda</span>
								</label>
							</div>
							<hr class="m-t-0 m-b-40">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Codice fiscale</label>
										<div class="col-md-9">
											<input id="fiscalCode" name="fiscalCode" type="text" class="form-control takeIn"  >
										</div>
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Partita Iva</label>
                                        <div class="col-md-9">
                                            <input id="piva" name="piva" type="text" class="form-control takeIn" input-check='equal11' >
                                        </div>
                                    </div>
                                </div>
							</div>
                            <!--/row-->
                            <!--row-->
							<div data-block='admin' data-role='centralino'>
								<h3 class="box-title">Associazione Consulente</h3>
								<hr class="m-t-0 m-b-40">
								<div class="row" >
									<div class="col-md-6">
										<div class="form-group">
											<span class="mytooltip tooltip-effect-2">
												<label class="control-label col-md-3"><span class="tooltip-item">Operatore</span></label>

												<span class="tooltip-content clearfix">
												  <span class="tooltip-text" style='padding-left:20px'>
												   <h3 style='color:white'>Storico operatori</h3>
												   <ul id="operatore_history">
												   {{for operatore_history  }}
													{{if (operatore != 'empty') }}<li>{{:operatore}}    -  {{:~formatUntil(until)}}  </li>
													{{/if}}
													{{/for}}
													</ul>
												  </span>
												</span>
											</span>
											<div class="col-md-9">
												<div class="input-group">
													<select id='operatore' class="form-control operatorAssoc takeIn" name='operatore'>
													<option></option>
													</select>
													<!-- <span class="input-group-btn"><button class="btn btn-default btn-outline" type="button">+</button></span> -->
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-3">Data primo Contatto</label>
											<div class="col-md-9">
												<div class="input-group">
													<input type="text" class="form-control takeIn generalTimePicker" id="data_contatto" name="data_contatto" placeholder="">
													<span class="input-group-addon"><i class="icon-calender"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <!--/row-->
                            <!--row-->
							 <!--INIZIO FILES-->
                            <h3 class="box-title">FILE CARICATI</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-6">Convalida Ricezione Documenti</label>
                                        <div class="col-md-6">
                                                <input id="checkReceived" name="isDocumentReceived" type="checkbox" class="  form-control takeIn" placeholder=""  data-block='backoffice'>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-6">Tipo File</label>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <select  id='nameFile' class="form-control " >
                                                    <option value="cartaIdentita">Carta identità</option>
                                                    <option value="codiceFiscale">Codice Fiscale</option>
                                                    <option value="privacy">Privacy</option>
                                                    <option value="other">Altro</option>

                                                </select>
                                                <!-- <span class="input-group-btn"><button class="btn btn-default btn-outline" type="button">+</button></span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" >
                                        <div class="form-group">
                                            <label class="control-label col-md-6">Data Ricezione Documenti</label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="text" class="form-control takeIn generalTimePicker" id="data_ricezione_documenti" name="data_ricezione_documenti">
                                                    <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="container">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Aggiungi files...</span>
                                                                                <!-- The file input field used as target for the file upload widget -->
                                            <!--<input id="fileupload" type="file" name="files[filecliente]" multiple>-->

                                             <input id="fileupload" type="file" name="fileCliente" >
                                        </span>
                                        <br>
                                        <br>
                                        <!-- The global progress bar -->
                                        <div style="max-width:40%" id="progress" class="progress">
                                            <div class="progress-bar progress-bar-success"></div>
                                        </div>
                                        <!-- The container for the uploaded files -->
                                        <div id="files" class="files"></div>
                                        <br>
                                        <div class="panel panel-default">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                         <div class="form-group">
                                            <label class="control-label col-md-6">Note in Archivio</label>
                                            <div class="col-md-6">
                                                <textarea id="note_archivio" name="note_archivio" class="form-control takeIn" rows="5"></textarea>

                                             </div>
                                         </div>
                                 </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-6">Posizione in Archivio</label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <select  id='posizione_archivio' class="form-control takeIn" name='posizione_archivio'>
                                                    </select>
                                                    <!-- <span class="input-group-btn"><button class="btn btn-default btn-outline" type="button">+</button></span> -->
                                                </div>
                                            </div>
                                        </div>
                                 </div>
                            </div>
                            <!--/row-->
                            <!--row-->

                            <!--BUTTON FOR FILES-->
                            <h3 class="box-title">Files Allegati</h3>
                            <hr class="m-t-0 m-b-40">
                            <div  class="row">
                                <div id="filesHolder" class="button-box">

                                 </div>
                            </div>
                            <hr class="m-t-0 m-b-40">
                            <!--/FINE FILE-->
                            <div class="form-actions">
                                <div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
												    {{if items == "create"}}
												    	<a href="javascript: $.fn.create();" type="button" class="btn btn-success">Crea</a>
												    {{else items == "edit"}}
												    	<a href="javascript: $.fn.save();" type="button" class="btn btn-success">Salva</a>
												    {{/if}}
													<a href="javascript:$.module.del();" data-role='admin' id="delBtn2" type="button" class="btn btn-default">Cancella</a>
													<button id="createClient" data-role='confermato' type="button" class="btn btn-warning">Crea Cliente</button>
												</div>
											</div>
										</div>
										<div class="col-md-6">

										</div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>`
);

$.templates("clientsRow",
    `<td>{{:clientCode onerror=''}}</td>
         <td> {{:companyId onerror=''}}</td>
         <td> {{:group     onerror=''}}</td>
         <td> {{:category  onerror=''}}</td>
         <td> {{:status    onerror=''}}</td>
         <td>
            <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','clients');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a data-role='admin'  href="javascript:$.module.del('clients','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>`
);